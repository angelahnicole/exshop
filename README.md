# EXSHOP #

A homework assignment for my Advanced Client-Side Web class that creates an ex-boyfriend / ex-girlfriend shopping website. The premise of this site is that you allow all those broken hearts to sell the stuff they received as gifts from their significant others. (Because all have it). You should allow the seller to sign up, and then list items. You are going to store their items in web storage and persist the data throughout the stay. Users should also be able to come in an view the items for sale and put them in a cart and purchase them. These should also be stored in web storage.

### Functionality ###

  1. Create an introduction page describing the purpose of the site.
  2. Create a page for sellers to sign up.
  3. Create a page where sellers can list items.
  4. Create a page where buyers can view the items and add them to a cart.
  5. Create a purchase and confirmation page.
  6. Items for sale should be stored in web storage
  7. Items put into the cart should be stored in web storage
  8. The confirmation page should read the web storage and display what they purchased.
  9. The form to sign up should validate the email, require the name, username and password.
  10. The form to purchase should validate credit cards (basic validation, 16 digits, starts with 4 with Visa, etc), check expiration date is past today

### Screenshots ###

-------------------------------------------------------------------------------------------

![EXSHOP_1-min.PNG](https://bitbucket.org/repo/6qRrMy/images/3737200064-EXSHOP_1-min.PNG)

-------------------------------------------------------------------------------------------

![EXSHOP_2-min.PNG](https://bitbucket.org/repo/6qRrMy/images/4233241671-EXSHOP_2-min.PNG)

-------------------------------------------------------------------------------------------

![EXSHOP_3-min.PNG](https://bitbucket.org/repo/6qRrMy/images/2054737259-EXSHOP_3-min.PNG)

-------------------------------------------------------------------------------------------

![EXSHOP_4-min.PNG](https://bitbucket.org/repo/6qRrMy/images/738986624-EXSHOP_4-min.PNG)

-------------------------------------------------------------------------------------------

![EXSHOP_5-min.PNG](https://bitbucket.org/repo/6qRrMy/images/842100775-EXSHOP_5-min.PNG)

-------------------------------------------------------------------------------------------

![EXSHOP_6-min.PNG](https://bitbucket.org/repo/6qRrMy/images/2182157124-EXSHOP_6-min.PNG)

-------------------------------------------------------------------------------------------

![EXSHOP_7-min.PNG](https://bitbucket.org/repo/6qRrMy/images/1627848469-EXSHOP_7-min.PNG)

-------------------------------------------------------------------------------------------

![EXSHOP_8-min.PNG](https://bitbucket.org/repo/6qRrMy/images/3298180277-EXSHOP_8-min.PNG)

-------------------------------------------------------------------------------------------