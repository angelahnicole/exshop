// ================================================================================================================================
// sell-products.js
// -------------------------------------------------------------------------------------------------------------------------------
// @author Angela Gross
// Advanced Client-Side Web Programming
// Homework 3
// -------------------------------------------------------------------------------------------------------------------------------
// Handles listing and adding user products.
// ================================================================================================================================

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// HTML for signing up
var formHTML = '<form class="form-horizontal" id="sign-up-form"> <fieldset> <!-- Form Name --> <legend>Sign Up</legend> <!-- Text input--> <div class="form-group"> <label class="col-md-4 control-label" for="username">Username</label> <div class="col-md-8"> <input id="username" name="username" type="text" placeholder="e.g. janesmith2000" class="form-control input-md"> <span class="help-block"> </span> </div> </div> <!-- Text input--> <div class="form-group"> <label class="col-md-4 control-label" for="fullname">Full Name</label> <div class="col-md-8"> <input id="fullname" name="fullname" type="text" placeholder="e.g. Jane Smith" class="form-control input-md"> <span class="help-block"> </span> </div> </div> <!-- Text input--> <div class="form-group"> <label class="col-md-4 control-label" for="email">Email</label> <div class="col-md-8"> <input id="email" name="email" type="text" placeholder="e.g. jane.smith@email.com" class="form-control input-md"> <span class="help-block"> </span> </div> </div> <!-- Password input--> <div class="form-group"> <label class="col-md-4 control-label" for="password">Password</label> <div class="col-md-8"> <input id="password" name="password" type="password" placeholder="*************************" class="form-control input-md"> <span class="help-block"> </span> </div> </div> <!-- Button --> <div class="form-group"> <label class="col-md-4 control-label sr-only" for="submit">Submit</label> <div class="col-md-4"> <button id="submit" name="submit" class="btn btn-primary">Submit</button> </div> </div> </fieldset> </form> ';

// HTML for listing and adding user products
var listHTML = '<h3>Products Your Selling</h3><div class="container-fluid" id="products"></div><hr><h3>Add a Product to Sell</h3><form class="form-horizontal" id="sell-list-form"> <fieldset> <div class="form-group"> <label class="col-md-3 control-label" for="image">Image</label> <div class="col-md-6"> <input id="image" name="image" type="file" class="form-control form-product-image" required/> <span class="help-block"> </span> </div><div class="col-md-3"><img src="" id="img-preview" class="img-responsive img-thumbnail" alt="Image preview..."></div> </div> <!-- Text input--> <div class="form-group"> <label class="col-md-3 control-label" for="name">Name</label> <div class="col-md-9"> <input id="name" name="name" type="text" placeholder="Used sweatshirt" class="form-control form-product-name" required> <span class="help-block"></span> </div> </div> <!-- Text input--> <div class="form-group"> <label class="col-md-3 control-label" for="price">Price</label> <div class="col-md-9"> <input id="price" name="price" type="number" placeholder="0.00" class="form-control form-product-price" required> <span class="help-block"> </span> </div> </div> <!-- Select Basic --> <div class="form-group"> <label class="col-md-3 control-label" for="type">Type</label> <div class="col-md-9"> <select id="type" name="type" class="form-control form-product-type" required> <option> </option> <option>Clothing</option> <option>Makeup</option> <option>Video Game</option> <option>Electronics</option> </select> <span class="help-block"> </span> </div> </div> <!-- Select Basic --> <div class="form-group no-display"> <label class="col-md-3 control-label" for="size">Size</label> <div class="col-md-9"> <select id="size" name="size" class="form-control form-clothing"> <option>S</option> <option>M</option> <option>L</option> <option>XL</option> </select> <span class="help-block"> </span> </div> </div> <!-- Select Basic --> <div class="form-group no-display"> <label class="col-md-3 control-label" for="condition">Condition</label> <div class="col-md-9"> <select id="condition" name="condition" class="form-control form-clothing form-video-game form-electronics"> <option>Very Used</option> <option>Fair</option> <option>Good</option> <option>Like New</option> </select> <span class="help-block"> </span> </div> </div> <!-- Select Basic --> <div class="form-group no-display"> <label class="col-md-3 control-label" for="makeup-type">Makeup Type</label> <div class="col-md-9"> <select id="makeup-type" name="makeup-type" class="form-control form-makeup"> <option>Lipstick / Lip Makeup</option> <option>Eyeshadow</option> <option>Foundation / Concealer / Primer / Face Makeup</option> <option>Eyeliner / Mascara / Eye Makeup</option> <option>Other</option> </select> <span class="help-block"> </span> </div> </div> <!-- Select Basic --> <div class="form-group no-display"> <label class="col-md-3 control-label" for="makeup-brand">Makeup Brand</label> <div class="col-md-9"> <input id="makeup-brand" name="makeup-brand" type="text" placeholder="Kat Von D" class="form-control form-makeup"> <span class="help-block"> </span> </div> </div> <!-- Select Basic --> <div class="form-group no-display"> <label class="col-md-3 control-label" for="makeup-fill">Makeup Fill</label> <div class="col-md-9"> <input id="makeup-fill" name="makeup-fill" type="number" max="100" placeholder="20" class="form-control form-makeup"> <span class="help-block"> </span> </div> </div> <!-- Select Basic --> <div class="form-group no-display"> <label class="col-md-3 control-label" for="platform">Platform</label> <div class="col-md-9"> <select id="platform" name="platform" class="form-control form-video-game"> <option>PS1</option> <option>PS2</option> <option>PS3</option> <option>PS4</option> <option>XBOX 1</option> <option>XBOX 360</option> <option>XBOX One</option> <option>Wii</option> <option>Wii U</option> <option>DS</option> <option>2DS</option> <option>3DS</option> <option>PC</option> <option>Other</option> </select> <span class="help-block"> </span> </div> </div> <!-- Select Basic --> <div class="form-group no-display"> <label class="col-md-3 control-label" for="elect-type">Electronic Type</label> <div class="col-md-9"> <select id="elect-type" name="elect-type" class="form-control form-electronics"> <option>Computer</option> <option>MP3 Player</option> <option>Appliance</option> <option>TV</option> <option>Phone</option> <option>Accessories</option> <option>Lights</option> </select> <span class="help-block"> </span> </div> </div> <!-- Button --> <div class="form-group"> <label class="col-md-3 control-label sr-only" for="submit">Add Item</label> <div class="col-md-9"> <button id="add-item" name="submit" class="btn btn-primary">Add Item</button> </div> </div> </fieldset> </form>';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ===========================================================================================================================
// LOAD SELL PAGE
// --------------------------------------------------------------------------------------------------------------------------
// Loads different HTML depending on whether or not the user is signed in.
// ===========================================================================================================================
function loadSellPage()
{
    var userSignedIn = $.jStorage.get('user');
    
    // Listing and adding user products
    if(userSignedIn)
    {
        buildSellList();
    }
    // Signing up
    else
    {
        buildSellSignUp();
    }
}

// ===========================================================================================================================
// BUILD SELL SIGNUP
// --------------------------------------------------------------------------------------------------------------------------
// Shows and validates sign up form.
// ===========================================================================================================================
function buildSellSignUp()
{
    $("#main-sell-panel").append(formHTML);
    
    // Overriding submit event on form
    $("#sign-up-form").submit(function(event) 
    {
        event.preventDefault();
        
        // Submit and validate form information to sign in user
        submitSignupForm(event);
    });

    // Validating and focusing the inputs on focus/blur events
    $("#sign-up-form input").focus(focusInput);
    $("#sign-up-form input").blur(function(event)
    {
        blurAndValidateInput(signupValidateFunctions, signupErrorTexts, this);
    });
}

// ===========================================================================================================================
// BUILD SELL LIST
// --------------------------------------------------------------------------------------------------------------------------
// Shows add product form and products that the user is selling.
// ===========================================================================================================================
function buildSellList()
{
    $("#main-sell-panel").append(listHTML);
    
    // Overriding submit event on form
    $("#sell-list-form").submit(function(event) 
    {
        event.preventDefault();
        
        // Add product and refresh products div
        addUserProduct();
        fillUserProducts();
       
    });
    
    // Bind onchange event for type
    $('#type').change(function(event)
    {
        // Hide type specific inputs
        $(".form-clothing").parent().parent().hide();
        $(".form-clothing").prop("required", false);
        $(".form-makeup").parent().parent().hide();
        $(".form-makeup").prop("required", false);
        $(".form-video-game").parent().parent().hide();
        $(".form-video-game").prop("required", false);
        $(".form-electronics").parent().parent().hide();
        $(".form-electronics").prop("required", false);

        // Show type specific inputs
        switch($(this).val())
        {
            case "Clothing":
                $(".form-clothing").parent().parent().show();
                $(".form-clothing").prop("required", true);
                break;
            
            case "Makeup":
                $(".form-makeup").parent().parent().show();
                $(".form-makeup").prop("required", true);
                break;
            
            case "Video Game":
                $(".form-video-game").parent().parent().show();
                $(".form-video-game").prop("required", true);
                break;
            
            case "Electronics":
                $(".form-electronics").parent().parent().show();
                $(".form-electronics").prop("required", true);
                break;
                
            default: break;
            
        }
    });
    
    // Refresh products div
    fillUserProducts();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ===========================================================================================================================
// FILL USER PRODUCTS
// --------------------------------------------------------------------------------------------------------------------------
// Fills the product div with the user's products and hides the add item button
// ===========================================================================================================================
function fillUserProducts()
{
    fillProducts('user-products');
    $(".cd-add-item").hide();
}

// ===========================================================================================================================
// ADD USER PRODUCT
// --------------------------------------------------------------------------------------------------------------------------
// Parses form and adds user product to local storage in user products and global products
// ===========================================================================================================================
function addUserProduct()
{
    // Get products from local storage
    var products = $.jStorage.get('products');
    var userProducts = $.jStorage.get('user-products');
    
    // Get base information from object
    var product =
    {
        "id": products["length"],
        "name": $("#name").val(),
        "price": Number($("#price").val()),
        "img": $("#img-preview").prop("src")
    };
    
    // Add variable information to object
    switch($("#type").val())
    {
        case "Clothing":

            var values = $(".form-clothing").map(function() { return $(this).val(); });
            
            // Size, Condition
            product["size"] = values[0];
            product["condition"] = values[1];
            
            // Update products
            products["clothing"].push(product);
            products["length"]++;
            
            // Update user products
            userProducts["clothing"].push(product);

            break;

        case "Makeup":
            
            var values = $(".form-makeup").map(function() { return $(this).val(); });
            
            // Type, Brand, Fill
            product["type"] = values[0];
            product["brand"] = values[1];
            product["fill"] = values[2];
            
            // Update products
            products["makeup"].push(product);
            products["length"]++;
            
            // Update user products
            userProducts["makeup"].push(product);
            
            break;

        case "Video Game":
            
            var values = $(".form-video-game").map(function() { return $(this).val(); });
            
            // Condition, Platform
            product["condition"] = values[0];
            product["platform"] = values[1];
            
            // Update products
            products["video_games"].push(product);
            products["length"]++;
            
            // Update user products
            userProducts["video_games"].push(product);
            
            break;

        case "Electronics":
            
            var values = $(".form-electronics").map(function() { return $(this).val(); });
            
            // Condition, Type
            product["condition"] = values[0];
            product["type"] = values[1];
            
            // Update products
            products["electronics"].push(product);
            products["length"]++;
            
            // Update user products
            userProducts["electronics"].push(product);
            
            break;

        default: break;
    }
    
    // Update local storage
    $.jStorage.set('products', products);
    $.jStorage.set('user-products', userProducts);
}

// ===========================================================================================================================
// PREVIEW FILE
// --------------------------------------------------------------------------------------------------------------------------
// Reads the file, gets the data url, and uses that to preview the file.
// ===========================================================================================================================
function previewFile() 
{
    // Check if the various File API support first before parsing
    if(!(window.File && window.FileReader && window.FileList && window.Blob))
    {
        alert('The File APIs are not fully supported by your browser.');
        return;
    }
    
    var preview = $("#img-preview");
    var file = $('#image').get(0).files[0];
    var reader = new FileReader();

    reader.onloadend = function() 
    {
      preview.attr("src", reader.result);
    }

    if(file) 
    {
      reader.readAsDataURL(file);
    } 
    else 
    {
      preview.attr("src", "");
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ===========================================================================================================================
// SIGN IN
// --------------------------------------------------------------------------------------------------------------------------
// Signs the user in by setting the user variable
// 
// @param {Object} user - Object that holds the user's information
// ===========================================================================================================================
function signIn(user)
{
    $.jStorage.set('user', user);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function()
{
    loadSellPage();
    
    $("#image").change(function(event)
    {
       previewFile(); 
    });
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////