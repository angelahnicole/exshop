// ================================================================================================================================
// validate.js
// -------------------------------------------------------------------------------------------------------------------------------
// @author Angela Gross
// Advanced Client-Side Web Programming
// Homework 3
// -------------------------------------------------------------------------------------------------------------------------------
// Validate JavaScript file for Homework 3
// ================================================================================================================================

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ATTRIBUTES

var notificationID = "#notifications";

// EMAIL REGEX
var EMAIL_PATTERN = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

// PASSWORD VALIDATION
var VALID_PASSWORD_MIN_LENGTH = 8;
var UPPER_CHAR_PATTERN = /[A-Z]+/;
var LOWER_CHAR_PATTERN = /[a-z]+/;
var ONE_DIGIT_PATTERN = /[0-9]+/;
var SPECIAL_CHAR_PATTERN = /(_|[^a-zA-Z0-9_])+/;

// DATE REGEX
var DATE_PATTERN = /^\d{2}([-\/])\d{4}$/;
var DATE_DELIMITERS_PATTERN = /[-\/]+/;

// VALIDATION FUNCTIONS APPLIED TO FORM ELEMENTS
var signupValidateFunctions = 
{
    "#username" : [ isNonEmpty ],
    "#fullname" : [ isNonEmpty ],
    "#email" : [ isNonEmpty, isValidEmailAddress ],
    "#password" : 
    [
        isValidPasswordLength,
        hasUpperChar,
        hasLowerChar,
        hasNumericDigit,
        hasSpecialChar
    ]
};
var checkoutValidateFunctions =
{
    "#email" : [ isNonEmpty, isValidEmailAddress ],
    "#fullname" : [ isNonEmpty ],
    "#address" : [ isNonEmpty ],
    "#cc-number" : [ isNonEmpty, isValidCreditCardNumber ],
    "#cc-exp" : [ isNonEmpty, isValidDate, isAfterToday ]
};

// ERRORS ADDED WHEN ASSOCIATED VALIDATION FUNCTION FAILS
var signupErrorTexts =
{
    "#email" : [ "Your email address is required. <br>", "The entered email address is not valid. <br>" ],
    "#fullname" : ["Your full name is required. <br>"],
    "#username" : [ "Your username is required. <br>" ],
    "#password" : 
    [
        "Your password needs at least " + VALID_PASSWORD_MIN_LENGTH + " characters. <br>", 
        "Your password needs at least one upper case letter. <br>", 
        "Your password needs at least one lower case letter. <br>",
        "Your password needs at least one numeric digit. <br>",
        "Your password needs at least one special character. <br>"
    ]
};
var checkoutErrorTexts =
{
    "#email" : [ "Your email address is required. <br>", "The entered email address is not valid. <br>" ],
    "#fullname" : ["Your full name is required. <br>"],
    "#address" : [ "Your address is required. <br>" ],
    "#cc-number" : [ "Your credit card number is required. <br>", "Your credit card number is not valid. <br>" ],
    "#cc-exp" : 
    [ 
        "The expiration date is required. <br>", 
        "The entered expiration date is not valid (mm-yyyy or mm/yyyy is valid). <br>", 
        "The entered expiration date is not after today's date. <br>" 
    ]
};

// TALLY OF WHETHER OR NOT THE FORM IS VALID
var isFormValid = false;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// USER SIGN UP EVENTS

// ===========================================================================================================================
// SUBMIT SIGN UP FORM
// --------------------------------------------------------------------------------------------------------------------------
// Function used to override the submit event for the sign up form.
// 
// @param {object} event
//  jQuery event object
// ===========================================================================================================================
function submitSignupForm(event)
{
    event.preventDefault();

    var isValid = validateSignupForm();

    // If it's valid, then add user the local storage
    if(isValid)
    {
        var user =
        {
            "username" : $("#username").val(),
            "fullname" : $("#fullname").val(),
            "email" : $("#email").val()
        };
        
        // Set user
        $.jStorage.set('user', user);
        
        // Set user products
        $.jStorage.set('user-products', 
        {
            "clothing": [],
            "makeup": [],
            "video_games": [],
            "electronics": [],
            "length": 0
        });
        
        location.reload();
    }
}

// ===========================================================================================================================
// SUBMIT CHECKOUT FORM
// --------------------------------------------------------------------------------------------------------------------------
// Function used to override the submit event for the checkout form.
// 
// @param {object} event
//  jQuery event object
// ===========================================================================================================================
function submitCheckoutForm(event)
{
    event.preventDefault();

    var isValid = validateCheckoutForm();

    // If it's valid
    if(isValid)
    {
        $(notificationID).show();
        $(notificationID).html("Success! Your credit card has been charged $" + $.jStorage.get('cart_total'));
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// INPUT MODIFICATION

// ===========================================================================================================================
// FOCUS INPUT
// --------------------------------------------------------------------------------------------------------------------------
// Toggles a "focus-input" class that changes the background color of the element.
// ===========================================================================================================================
function focusInput()
{
    $(this).toggleClass("focus-input");
}

// ===========================================================================================================================
// BLUR AND VALIDATE INPUT
// --------------------------------------------------------------------------------------------------------------------------
// Toggles the "focusInput" class and then validates the form element. To be used as the blur event for all inputs.
// ===========================================================================================================================
function blurAndValidateInput(validateFunctions, errorTexts, domElement)
{
    // Focus
    focusInput();
    
    // Reset notifications
    $(notificationID).hide();
    
    // Get key and value
    var validateTuple =
    {
        key : '#' + $(domElement).attr('id'),
        value : $(domElement).val()      
    };
    
    // Validate input
   isFormValid = validateInput(validateTuple, validateFunctions[validateTuple["key"]], errorTexts[validateTuple["key"]]) && isFormValid;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// UTILITY INPUT VALIDATION

// ===========================================================================================================================
// VALIDATE SIGN UP FORM
// --------------------------------------------------------------------------------------------------------------------------
// Validates the whole form (the blur event for each input validates the input).
// 
// @returns {bool}
//  Whether or not the form is valid.
// ===========================================================================================================================
function validateSignupForm()
{
    // Check once
    if(!isFormValid)
    {
        // Reset form valid boolean
        isFormValid = true;
        
        // Validate everything
        $("#sign-up-form input").blur();
    }
    
    return isFormValid;
}

// ===========================================================================================================================
// VALIDATE CHECKOUT FORM
// --------------------------------------------------------------------------------------------------------------------------
// Validates the whole form (the blur event for each input validates the input).
// 
// @returns {bool}
//  Whether or not the form is valid.
// ===========================================================================================================================
function validateCheckoutForm()
{
    // Check once
    if(!isFormValid)
    {
        // Reset form valid boolean
        isFormValid = true;
        
        // Validate everything
        $("#checkout-form input").blur();
    }
    
    return isFormValid;
}

// ===========================================================================================================================
// VALIDATE INPUT
// --------------------------------------------------------------------------------------------------------------------------
// Checks the given form element for errors. If there are errors, it formats the form element to alert the user that there 
// are errors. If validation was successful, then it it formats the form element to alert the user that they entered their
// information in correctly.
// 
// @param {object} validateTuple
//  Tuple of form ID and value
//  
//  @param {array} validateFunctions
//  Functions used to validate the element
//  
// @param {array} errorTexts
//  Texts to display for error
//  
// @param {string} notificationID
//  ID of the given HTML element to display the notification text(# needs to be included)
//  
// @return {bool} 
//  Whether or not the form element is valid
// ===========================================================================================================================
function validateInput(validateTuple, validateFunctions, errorTexts)
{   
    // Initialize helper variables
    var totalValidInput = true;
    var validateFunction = null;
    var validInput = false;
    var validationClass = "";
    var helpText = "";
    
    // Retrieve key and value pairs
    var validateKey = validateTuple["key"];
    var validateValue = validateTuple["value"];
    
    // Reset help text
    $(validateKey).parent().find("span.help-block").text("");
    
    // Use all validation functions to validate the form element
    for(var i = 0; i < validateFunctions.length; i++)
    {
        // Validate input
        validateFunction = validateFunctions[i];
        validInput = validateFunction(validateValue, validateKey);
        
        // If there was an error, add the error to the help box
        if(!validInput)
        {
            helpText = $(validateKey).parent().find("span.help-block").html();
            $(validateKey).parent().find("span.help-block").html(helpText + errorTexts[i]);
        }
        
        // Accumulate validation
        totalValidInput = totalValidInput && validInput;
    }
    
    // Remove old validation classes
    $(validateKey).parent().removeClass("has-success");
    $(validateKey).parent().removeClass("has-error");

    // Add validation classes
    validationClass = totalValidInput ? "has-success" : "has-error";
    $(validateKey).parent().addClass(validationClass);
    
    return totalValidInput;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// VALIDATION FUNCTIONS

// ===========================================================================================================================
// IS NON EMPTY
// --------------------------------------------------------------------------------------------------------------------------
// Returns whether or not the value is non-empty
// 
// @param {string} value
//  Value you want to validate
//  
// @return {bool} 
//  Whether or not it's non-empty
// ===========================================================================================================================
function isNonEmpty(value)
{
    return value.length > 0;
}

// ===========================================================================================================================
// IS VALID EMAIL ADDRESS
// --------------------------------------------------------------------------------------------------------------------------
// Returns whether or not the value is a valid email address
// 
// @param {string} value
//  Value you want to validate
//  
// @return {bool} 
//  Whether or not it's a valid email address
// ===========================================================================================================================
function isValidEmailAddress(value) 
{
    return EMAIL_PATTERN.test(value);
};

// ===========================================================================================================================
// IS VALID PASSWORD LENGTH
// --------------------------------------------------------------------------------------------------------------------------
// Returns whether or not the value is at least as long as the minimum password length
// 
// @param {string} value
//  Value you want to validate
//  
// @return {bool} 
//  Whether or not it is long enough
// ===========================================================================================================================
function isValidPasswordLength(value)
{
    return value.length >= VALID_PASSWORD_MIN_LENGTH;
}

// ===========================================================================================================================
// HAS UPPER CHAR
// --------------------------------------------------------------------------------------------------------------------------
// Returns whether or not the value has an upper case character
// 
// @param {string} value
//  Value you want to validate
//  
// @return {bool} 
//  Whether or not it has an upper case character
// ===========================================================================================================================
function hasUpperChar(value)
{
    return UPPER_CHAR_PATTERN.test(value);
}

// ===========================================================================================================================
// HAS LOWER CHAR
// --------------------------------------------------------------------------------------------------------------------------
// Returns whether or not the value has a lower case character
// 
// @param {string} value
//  Value you want to validate
//  
// @return {bool} 
//  Whether or not it has a lower case character
// ===========================================================================================================================
function hasLowerChar(value)
{
    return LOWER_CHAR_PATTERN.test(value);
}

// ===========================================================================================================================
// HAS NUMERIC DIGIT
// --------------------------------------------------------------------------------------------------------------------------
// Returns whether or not the value has a numeric digit
// 
// @param {string} value
//  Value you want to validate
//  
// @return {bool} 
//  Whether or not it has a numeric digit
// ===========================================================================================================================
function hasNumericDigit(value)
{
    return ONE_DIGIT_PATTERN.test(value);
}

// ===========================================================================================================================
// HAS SPECIAL CHAR
// --------------------------------------------------------------------------------------------------------------------------
// Returns whether or not the value has a special character
// 
// @param {string} value
//  Value you want to validate
//  
// @return {bool} 
//  Whether or not it has a special character
// ===========================================================================================================================
function hasSpecialChar(value)
{
    return SPECIAL_CHAR_PATTERN.test(value);
}

// ===========================================================================================================================
// IS VALID CREDIT CARD NUMBER
// --------------------------------------------------------------------------------------------------------------------------
// Returns whether or not the number is a valid credit card number
// 
// @param {string} value
//  Value you want to validate
//  
// @param {string} validateKey
//  Key of the element that holds the credit card number
//  
// @return {bool} 
//  Whether or not it is a valid credit card number
// ===========================================================================================================================
function isValidCreditCardNumber(value, validateKey)
{ 
    return $(validateKey).validateCreditCard().valid;
}

// ===========================================================================================================================
// IS VALID DATE
// --------------------------------------------------------------------------------------------------------------------------
// Returns whether or not it's a valid date (mm-yyyy or mm/yyyy)
// 
// @param {string} value
//  Value you want to validate
//  
// @return {bool} 
//  Whether or not it is a date
// ===========================================================================================================================
function isValidDate(value)
{
    return DATE_PATTERN.test(value);
}

// ===========================================================================================================================
// IS AFTER TODAY
// --------------------------------------------------------------------------------------------------------------------------
// For a date formatted like mm-yyyy or mm/yyyy, it checks if the date is after today
// 
// @param {string} value
//  Value you want to validate
//  
// @return {bool} 
//  Whether or not the date is after today
// ===========================================================================================================================
function isAfterToday(value)
{
    // Get today's date
    var today = new Date();
    var mm = today.getMonth() + 1; // January is 0!
    var yyyy = today.getFullYear();
    
    var date = value.split(DATE_DELIMITERS_PATTERN); // 0: mm, 1: yyyy
    
    // If the year is greater than today, then we're good
    if(date[1] > yyyy)
    {
        return true;
    }
    // If it's equal, then we have to check the month
    else if(date[1] == yyyy)
    {
        return date[0] >= mm;
    }
    else
    {
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////