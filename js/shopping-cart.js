// ================================================================================================================================
// shopping-cart.js
// -------------------------------------------------------------------------------------------------------------------------------
// @author Angela Gross
// Advanced Client-Side Web Programming
// Homework 3
// -------------------------------------------------------------------------------------------------------------------------------
// Handles products and the shopping cart for the website
// ================================================================================================================================

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Path of product JSON file
var productPath = "./data/products.json";

// Product grid format strings
var productColFormatStr = '<div class="col-lg-3 product" id="product-{0}"> <div class="border"><div class="desc"></div></div></div>';
var productIDFormat = "#product-{0}";

// Cart format strings
var cartProductFormatStr = '<li> <span class="cd-qty">{0}</span> {1} <div class="cd-price">{0} x ${2} = ${3}</div> <a href="#" product_id="{4}" class="cd-item-remove cd-img-replace">Remove</a> </li>';
var cartTotalFormatStr = '<p>Total <span>${0}</span></p>';

// Product format string
var clothingFormatStr = '<h3 class="product-name">{0}</h3> <span class="product-price">${1}</span> <span class="product-condition">{2}</span> <span class="product-info">{3}</span> <span product_id="{4}" class="fa fa-2x fa-plus-square cd-add-item"></span>';
var makeupFormatStr = '<h3 class="product-name">{0}</h3> <span class="product-price">${1}</span> <span class="product-info">{2}</span> <span class="product-info">{3}</span> <span class="product-info">{4}</span> <span product_id="{5}" class="fa fa-2x fa-plus-square cd-add-item"></span>';
var videoGamesFormatStr = '<h3 class="product-name">{0}</h3> <span class="product-price">${1}</span> <span class="product-info">{2}</span> <span class="product-info">{3}</span> <span product_id="{4}" class="fa fa-2x fa-plus-square cd-add-item"></span>';
var electronicsFormatStr = '<h3 class="product-name">{0}</h3> <span class="product-price">${1}</span> <span class="product-info">{2}</span> <span class="product-info">{3}</span> <span product_id="{4}" class="fa fa-2x fa-plus-square cd-add-item"></span>';


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ===========================================================================================================================
// INIT CART
// --------------------------------------------------------------------------------------------------------------------------
// Initializes the cart and cart total in local storage.
// ===========================================================================================================================
function initCart()
{
    $.jStorage.set('cart', []);
    $.jStorage.set('cart_total', 0.0);
}

// ===========================================================================================================================
// INIT PRODUCTS
// --------------------------------------------------------------------------------------------------------------------------
// Initializes the products with data from the product JSON file.
// 
// @param {bool} bFillProducts - Whether or not we fill the cart with products after initialization
// ===========================================================================================================================
function initProducts(bFillProducts)
{
    $.getJSON(productPath, function(data)
    {
        $.jStorage.set('products', data);
        
        if(bFillProducts) fillProducts();
    }); 
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ===========================================================================================================================
// FILL PRODUCTS
// --------------------------------------------------------------------------------------------------------------------------
// Fills the product grid from local storage
// 
// @param {string} productKey - [Optional] Key of products to fill in local storage (default is 'products')
// ===========================================================================================================================
function fillProducts(productKey)
{
    productKey = productKey || 'products';
    
    // Retrieve products
    var products = $.jStorage.get(productKey);
    
    // Get different types of products
    var clothing = products["clothing"];
    var makeup = products["makeup"];
    var video_games = products["video_games"];
    var electronics = products["electronics"];

    // Keep track of what product we're filling
    var productNum = 0;
    
    // Clear products first
    $("#products").html("");

    // Fill clothing products
    $.each(clothing, function(key, value)
    {
        // Gather relevant information to add to product grid
        var descElemHTML = clothingFormatStr.format(value["name"], value["price"], "Condition: " + value["condition"], "Size: " + value["size"], value["id"]);
        var productImg = value["img"];
        
        // Add product to grid
        addItemToProductGrid(descElemHTML, productImg, productNum);

        productNum++;
    });

    // Fill makeup products
    $.each(makeup, function(key, value)
    {
        // Gather relevant information to add to product grid
        var descElemHTML = makeupFormatStr.format(value["name"], value["price"], "Fill: " + value["fill"], "Brand: " +  value["brand"], "Type: " + value["type"], value["id"]);
        var productImg = value["img"];
        
        // Add product to grid
        addItemToProductGrid(descElemHTML, productImg, productNum);

        productNum++;
    });

    // Fill video game products
    $.each(video_games, function(key, value)
    {
        // Gather relevant information to add to product grid
        var descElemHTML = videoGamesFormatStr.format(value["name"], value["price"], "Condition: " + value["condition"], "Platform: " + value["platform"], value["id"]);
        var productImg = value["img"];
        
        // Add product to grid
        addItemToProductGrid(descElemHTML, productImg, productNum);

        productNum++;
    });

    // Fill electronic products
    $.each(electronics, function(key, value)
    {
        // Gather relevant information to add to product grid
        var descElemHTML = electronicsFormatStr.format(value["name"], value["price"], "Condition: " + value["condition"], "Type: " + value["type"], value["id"]);
        var productImg = value["img"];
        
        // Add product to grid
        addItemToProductGrid(descElemHTML, productImg, productNum);

        productNum++;
    });
    
    // Bind event to click
    $(".cd-add-item").click(function()
    {
       var productID = Number($(this).attr("product_id"));
       addItemToCart(productID);
    });
    
    // Bind events on enter and leave
    $(".product").mouseenter(function() 
    {
       $(this).find('.desc').slideToggle();

    });

    $(".product").mouseleave(function() 
    {
       $(this).find('.desc').slideToggle();
    });
}

// ===========================================================================================================================
// ADD ITEM TO PRODUCT GRID
// --------------------------------------------------------------------------------------------------------------------------
// Adds an item (column in a row) to the product grid.
// 
// @param {string} descElemHTML - Formatted HTML for the description HTML element with product information inside
// @param {string} productImg - Image path for the product
// @param {int} productNum - Number of the product that we're on in the grid
// ===========================================================================================================================
function addItemToProductGrid(descElemHTML, productImg, productNum)
{
    var productGrid = $("#products");
    var productID = productIDFormat.format(productNum);
    
    // Start new row if we need to
    if(productNum % 4 === 0)
    {
        productGrid.find(".current-row").removeClass("current-row");
        productGrid.append('<div class="row current-row"></div>');
    }
    
    // Get current row
    var currentRow = productGrid.find(".current-row");
    
    // Append column structure to row
    currentRow.append(productColFormatStr.format(productNum));
    
    // Get product element
    var productElem = $(productID);
    var descElem = productElem.find(".desc");

    // Fill product column with information
    descElem.html(descElemHTML);
    productElem.css("background-image", "url(" + productImg + ")");
}

// ===========================================================================================================================
// FILL CART
// --------------------------------------------------------------------------------------------------------------------------
// Fills the cart with items in local storage
// ===========================================================================================================================
function fillCart()
{
    var cart = $.jStorage.get('cart');
    var cartElem = $("#cd-cart");
    var cartListElem = cartElem.find(".cd-cart-items");
    var cartTotalElem = cartElem.find(".cd-cart-total");
    
    // Clear list
    cartListElem.html("");
    
    // Add products to the cart list
    $.each(cart, function(key, value)
    {
        var item = cartProductFormatStr.format(value["quantity"], value["name"], value["price"], value["total_price"], value["id"]);
        cartListElem.append(item);
    });
    
    // Update cart total
    cartTotalElem.html(cartTotalFormatStr.format($.jStorage.get('cart_total'))); 
    
    // Bind remove event to all
    $(".cd-item-remove").click(function()
    {
       var productID = Number($(this).attr("product_id"));
       removeItemFromCart(productID);
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ===========================================================================================================================
// ADD ITEM TO CART
// --------------------------------------------------------------------------------------------------------------------------
// Adds an item to the cart in local storage. It first looks for it and adds to the quantity if found, but if it doesn't
// find it then we push it to the cart, and then refreshes the cart.
// 
// @param {int} productID - ID that uniquely identifies the product
// ===========================================================================================================================
function addItemToCart(productID)
{
    var foundProduct = false;
    
    // Get the cart from storage
    var cart = $.jStorage.get('cart');
    var totalPrice = $.jStorage.get('cart_total');
    
    // Look for the product first
    $.each(cart, function(key, value)
    {
        // Update the pre-existing product
        if(value && value["id"] === productID)
        {
            totalPrice += value["price"];
            
            value["quantity"]++;
            value["total_price"] += value["price"];
            
            foundProduct = true;
        }
    });
    
    // If we didn't find the product, we retrieve the product and push it to the cart
    if(!foundProduct)
    {
        var product = getProduct(productID);
        
        totalPrice += product["price"];
        
        cart.push
        (
            {
                "id" : product["id"],
                "quantity" : 1,
                "price" : product["price"],
                "total_price" : product["price"],
                "name" : product["name"]
            }
        );
    }
    
    // Update cart
    $.jStorage.set('cart', cart);
    $.jStorage.set('cart_total', totalPrice);
    
    // Fill the shopping cart again
    fillCart();
}

// ===========================================================================================================================
// REMOVE ITEM FROM CART
// --------------------------------------------------------------------------------------------------------------------------
// Removes an item from the cart in local storage. It first looks for it and subtracts from the quantity if found, but if it doesn't
// find it then we splice it from the cart.
// 
// @param {int} productID - ID that uniquely identifies the product
// ===========================================================================================================================
function removeItemFromCart(productID)
{
    // Get the cart from storage
    var cart = $.jStorage.get('cart');
    var totalPrice = $.jStorage.get('cart_total');
    
    // Look for the product to delete
    $.each(cart, function(key, value)
    {
        console.log(value);
        
        // Update the pre-existing product
        if(value && value["id"] === productID)
        {
            // Update total price and quantity
            totalPrice -= value["price"];
            value["quantity"]--;
            value["total_price"] -= value["price"];
            
            // If quantity is zero, delete it
            if(value["quantity"] <= 0)
            {
                cart.splice(key, 1);
            }
        }
    });
    
    // Update cart
    $.jStorage.set('cart', cart);
    $.jStorage.set('cart_total', totalPrice);
    
    // Fill the shopping cart again
    fillCart();
}

// ===========================================================================================================================
// GET PRODUCT
// --------------------------------------------------------------------------------------------------------------------------
// Retrieves an item from products in local storage.
// 
// @param {int} productID - ID that uniquely identifies the product
// ===========================================================================================================================
function getProduct(productID)
{
    // Get products from storage
    var products = $.jStorage.get('products');
    
    // Try to find the product in clothing, makeup, video games, and electronics
    var product = $.grep(products.clothing, function(elem){ return elem["id"] === productID; });
    if(product.length === 0) product = $.grep(products.makeup, function(elem){ return elem["id"] === productID; });
    if(product.length === 0) product = $.grep(products.video_games, function(elem){ return elem["id"] === productID; });
    if(product.length === 0) product = $.grep(products.electronics, function(elem){ return elem["id"] === productID; });
    
    return product[0];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function()
{   
    // Initialize cart and cart total if they don't exist
    if(!$.jStorage.get('cart', false))
    {
        initCart();
    }
    
    // Initialize products if they don't exist
    if(!$.jStorage.get('products', false))
    {
        initProducts(false);
    }
    
    // Fill the cart
    fillCart();
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////